# Bat, a `cat(1)` clone with wings

This Ansible role installs [bat](https://github.com/sharkdp/bat), a drop-in
`cat` replacement with syntax highlighting.

## Installation

Add the following to a `requirements.yml` or `requirements.yaml` file in your
Ansible project and run `ansible-galaxy install -r requirements.yaml`:

```yaml
collections:
  - community.general

roles:
  - name: bat
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/bat.git
```


## Usage

Place a section like this in your playbook:

```yaml
- name: Set up bat
  hosts: all
  roles:
    - role: bat
```
### Additional options

Full argument documentation is available via `ansible-doc -t role bat`.
